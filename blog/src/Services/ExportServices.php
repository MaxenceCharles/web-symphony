<?php

namespace App\Services;

use App\Entity\Post;
use App\Form\PostType;
use Doctrine\ORM\EntityManagerInterface;

class ExportServices{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function export_Post()
    {
        /**@var Post[] $list */
        $list = $this->em->getRepository(Post::class)->findAll();

        $fp = fopen('export.csv', 'w');

        foreach($list as $fields){
            fputcsv($fp, $fields->toArray());
        }

        fclose($fp);
    

    }
}
